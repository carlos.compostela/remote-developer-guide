# Changelog

[comment]: <> (Formato del changelog: https://keepachangelog.com/es-ES/1.0.0/)

Todos los cambios más relevantes serán reflejados en este fichero.

## 2020-05-08
### Added
- (EXPERIMENTAL) Primera versión de la guía en formato epub.
- Imagen de carátula para PDF y EPUB.
- Añadidas herramientas KDE Connect y GSConnect en apartado de productividad.
- Instalación y uso de mssql-cli en el apartado de base de datos.
- Añadidas alternativas para videoconferencia.

### Changed
- Cambios en maquetación HTML (menú lateral) y en imágenes.
- Ampliada información en DBeaver.
- Ampliada información sobre doctl, gestión y securización de droplets en Digital Ocean.

## 2020-05-01
### Added
- Añadido fichero CHANGELOG.md (este mismo que estás a leer)
- Fichero docker-compose.yml para descargar la configuración para gitea.
- Apartado de Técnica pomodoro y herramientas de productividad (Super productivity, pomotroid y pomo.sh)
- Apartado de Herramientas de Escritorio Remoto (NoMachine y Remmina)

### Changed
- Información sobre la configuración de Dbeaver sobre SSH.
- Información sobre docker-compose y gitea.
- Corregida sección de información sobre el editor Atom.
- Corregido párrafo en gestores de paquetes software.
- Pantallazo de CopyQ en apartado de herramientas de productividad
- Logotipo de Gitea