# Guía para desarrolladores remotos

Guía para desarrolladores que trabajan en remoto. Los contenidos se publican con GitLab Pages en: https://vifito.gitlab.io/remote-developer-guide

## Contruír la guía

Para construír la guía se emplea la imagen de AsciiDoc.

```bash
# Clonar repositorio
git clone https://gitlab.com/vifito/remote-developer-guide.git

# Directorio del proyecto
cd remote-developer-guide

# Descargar imagen docker con AsciiDoctor
docker pull asciidoctor/docker-asciidoctor:latest

# Lanzar el contenedor
docker run --name asciidoc -it -v `pwd`:/documents/ --rm asciidoctor/docker-asciidoctor

# Construír HTML y PDF
make

# Salir del contenedor
exit
```