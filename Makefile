BOOKNAME=index
SOURCE=./adoc/$(BOOKNAME).adoc
TARGET_PDF=./public/pdf/remote-developer-guide.pdf

release: clean pdf html epub

pdf:
	mkdir -p ./public/pdf
	asciidoctor --trace -r asciidoctor-pdf -b pdf -dbook -a lang=es -a data-uri! -a pdf-style=resources/themes/guide-theme.yml -a pdf-fontsdir=resources/fonts --out-file $(TARGET_PDF) $(SOURCE) 

html:
	mkdir -p ./public/images ./public/videos ./public/files
	cp -Rf ./adoc/images/* ./public/images/
	cp -Rf ./adoc/videos/* ./public/videos/
	cp -Rf ./adoc/files/* ./public/files/
	asciidoctor -a linkcss --doctype book --source-dir ./adoc --destination-dir ./public $(SOURCE)

epub:
	mkdir -p ./public/epub
	asciidoctor-epub3 -a ebook -a listing-caption! -D ./public/epub $(SOURCE)

clean:
	rm -Rf ./public/*
